import pygame
from snake_game import SnakeGame

pygame.init()
# Colors

if __name__ == '__main__':
    # Set font and initialize game
    game = SnakeGame()
    restart = False  # Set restart flag to False

    while True:
        # If not restarting the game
        if not restart:  
            game_over, score = game.play_step()  
            # If the game is over
            if game_over:  
                while True:
                    for event in pygame.event.get():
                        if event.type == pygame.QUIT:
                            pygame.quit()
                            quit()
                        elif event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_r:  
                                restart = True  
                                break
                            elif event.key == pygame.K_q:  
                                pygame.quit()
                                quit()
                    if restart:
                        # Break the loop if restart flag is True
                        break  
        # If restarting the game
        else:  
            game = SnakeGame()  
            restart = False  
            continue  
