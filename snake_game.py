import pygame
import random
from collections import namedtuple
from direction import *

WHITE = (255, 255, 255)
BLOCK_SIZE = 20
SPEED = 3

Point = namedtuple('Point', 'x, y')


class SnakeGame:
    
    def __init__(self, w=640, h=480):
        # Display
        self.font = pygame.font.Font('font\Montserrat-Black.otf', 25)
        self.w = w
        self.h = h
        self.display = pygame.display.set_mode((self.w, self.h))
        pygame.display.set_caption('Snake')
        self.clock = pygame.time.Clock()
        # Choose direction
        self.direction = Direction.RIGHT
        # Graphics
        self.background = pygame.image.load('graphics\sky.jpg')
        self.background = pygame.transform.scale(self.background, (self.w, self.h))
        self.head_img = pygame.image.load('graphics\snkae_head.png')
        self.head_img = pygame.transform.scale(self.head_img, (BLOCK_SIZE, BLOCK_SIZE))
        self.snake_img = pygame.image.load('graphics\snkae_body.png')
        self.snake_img = pygame.transform.scale(self.snake_img, (BLOCK_SIZE, BLOCK_SIZE))
        self.food_img = pygame.image.load('graphics\straw.png')
        self.food_img = pygame.transform.scale(self.food_img, (BLOCK_SIZE, BLOCK_SIZE))
        # Audio effects
        self.eating_sound = pygame.mixer.Sound('audio\eating-effect.mp3')

        # Snake body 
        self.head = Point(self.w/2, self.h/2)
        self.snake = [self.head, 
                      Point(self.head.x-BLOCK_SIZE, self.head.y),
                      Point(self.head.x-(2*BLOCK_SIZE), self.head.y)]
        
        self.score = 0
        self.food = None
        self.angle = 0
        self.paused = False  
        self.game_over = False  
        self.show_start_screen()  
        self._place_food()  

    def _place_food(self):
        # Placing food on the game board
        x = random.randint(0, (self.w-BLOCK_SIZE )//BLOCK_SIZE )*BLOCK_SIZE 
        y = random.randint(0, (self.h-BLOCK_SIZE )//BLOCK_SIZE )*BLOCK_SIZE
        self.food = Point(x, y)
        if self.food in self.snake:
            self._place_food()
    def play_step(self):
        # Game mechanism
        for event in pygame.event.get():
            # Quit 
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                # Pause during game
                if event.key == pygame.K_SPACE:  \
                    self.paused = not self.paused
                # Game session, moving mechanism 
                if not self.paused:  
                    if event.key == pygame.K_LEFT:
                        self.direction = Direction.LEFT
                    elif event.key == pygame.K_RIGHT:
                        self.direction = Direction.RIGHT
                    elif event.key == pygame.K_UP:
                        self.direction = Direction.UP
                    elif event.key == pygame.K_DOWN:
                        self.direction = Direction.DOWN
        
        if not self.paused and not self.game_over:  
            self._move(self.direction)
            self.snake.insert(0, self.head)
        
            if self._is_collision():
                self.game_over = True
            elif self.head == self.food:
                self.score += 1
                self._place_food()
                self.eating_sound.play()
            else:
                self.snake.pop()
        
        self._update_ui()
        self.clock.tick(SPEED)
        return self.game_over, self.score
    
    def _is_collision(self):
        # Collision with body 
        if self.head.x >= self.w or self.head.x < 0 or self.head.y >= self.h or self.head.y < 0:
            self.eating_sound.play()
            return True
        if self.head in self.snake[1:]:
            return True
        return False
        
    def _update_ui(self):
        # Display 
        self.display.blit(self.background, (0,0))
        # Animation of snake's head
        rotated_snake_img = pygame.transform.rotate(self.head_img, self.angle)
        self.display.blit(rotated_snake_img, (self.snake[0].x, self.snake[0].y))     
        self.angle = (self.angle + 90) % 360
        
        for pt in self.snake[1:]:
            self.display.blit(self.snake_img, (pt.x, pt.y))
        # Food display
        self.display.blit(self.food_img, (self.food.x, self.food.y))           
        text = self.font.render("Score: " + str(self.score), True, WHITE)
        self.display.blit(text, [0, 0])
        
        # Pause sign
        if self.paused:  
            paused_text = self.font.render("Paused", True, WHITE)
            self.display.blit(paused_text, [self.w // 2 - paused_text.get_width() // 2, self.h // 2])
        
        # Game over sign
        if self.game_over:  
            game_over_text = self.font.render("Game Over", True, WHITE)
            self.display.blit(game_over_text, [self.w // 2 - game_over_text.get_width() // 2, self.h // 2])
            restart_text = self.font.render("Press R to Restart or Q to Quit", True, WHITE)
            self.display.blit(restart_text, [self.w // 2 - restart_text.get_width() // 2, self.h // 2 + 50])
        
        pygame.display.flip()
    
    # Move function     
    def _move(self, direction):
        # Pointing
        x = self.head.x
        y = self.head.y
        if direction == Direction.RIGHT:
            x += BLOCK_SIZE
        elif direction == Direction.LEFT:
            x -= BLOCK_SIZE
        elif direction == Direction.DOWN:
            y += BLOCK_SIZE
        elif direction == Direction.UP:
            y -= BLOCK_SIZE

        # Check if snake hit the wall 
        if x >= self.w:
            # Moves snakes to simetric, opposite place 
            x = 0  
        elif x < 0:
            # Moves snakes to simetric, opposite place 
            x = self.w - BLOCK_SIZE  
        elif y >= self.h:
            # Moves snakes to simetric, opposite place 
            y = 0  
        elif y < 0:
            # Moves snakes to simetric, opposite place 
            y = self.h - BLOCK_SIZE  

        self.head = Point(x, y)
    
    def show_start_screen(self):
        # Welcome screen event
        self.display.blit(self.background, (0,0))
        start_text = self.font.render("Press SPACE to start", True, (WHITE))
        self.display.blit(start_text, (200,240))
        pygame.display.flip()
        
        # Nothing happened until K_SPACE will be pushed
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                    return
